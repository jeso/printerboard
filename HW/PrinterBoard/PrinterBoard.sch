EESchema Schematic File Version 4
LIBS:PrinterBoard-cache
EELAYER 26 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3600 4400 1500 1200
U 5C46B167
F0 "PrinterBoard_MCU" 50
F1 "PrinterBoard_MCU.sch" 50
$EndSheet
Text Notes 3900 5000 0    50   ~ 0
Page 01: Microcontroller
$Sheet
S 5200 4400 1500 1200
U 5C480BFA
F0 "PrinterBoard_Drivers" 50
F1 "PrinterBoard_Drivers.sch" 50
$EndSheet
Text Notes 5650 5000 0    50   ~ 0
Page 02: Drivers
$Sheet
S 6800 4400 1500 1200
U 5C83D846
F0 "PrinterBoard_Connectors" 50
F1 "PrinterBoard_Connectors.sch" 50
$EndSheet
Text Notes 7000 5000 0    50   ~ 0
Page 03: Standard connectors
$Sheet
S 8400 4400 1450 1200
U 5C95BEF1
F0 "PrinterBoard_PowerConenctors" 50
F1 "PrinterBoard_PowerConnector.sch" 50
$EndSheet
Text Notes 8600 5000 0    50   ~ 0
Page 04: Power Connectors
$EndSCHEMATC
